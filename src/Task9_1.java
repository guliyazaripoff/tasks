
import static java.util.Arrays.deepToString;
//1.Заполнить массив из двенадцати элементов 1,2,...12.
// 2.Вывести элементы массива на экран в обратном порядке.
// 3.Определить:
// 1.сумму всех элементов массива;
// 2.произведение всех элементов массива;
// 3.сумму квадратов всех элементов массива;
// 4.сумму шести первых элементов массива
public class Task9_1 {
    public static void main(String[] args) {
        int n = 12;

        Integer[] array = new Integer[n];

        for (int i = 0; i < n; i++) {
            array[i] = i + 1;

        }
        for (int i = n - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        int s = 0;
        int p = 1;
        int k = 0;
        int s2 = 0;
        for (int i = 0; i < n; i++) {
            s += array[i];
            p *= array[i];
            k += array[i] * array[i];
            if (i < 5) {
                s2 += array[i];
            }
        }
        System.out.println("Сумма элементов: "+s);
        System.out.println("Произведение элементов: "+p);
        System.out.println("Сумма квадратов: "+k);
        System.out.println("Сумма шести первых элементов: "+s2);
    }
}