import java.util.Scanner;
public class Task6_4 {
    //Вычислить сумму 1+1/2+1/3+...+1/n
    public static void main(String[] args) {
        System.out.println("Введите n");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        double sum = 0;
        for (int i = 1; i <= n; i++){
            sum += 1.0/i;
        }
        System.out.println(sum);
    }
}
