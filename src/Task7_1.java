public class Task7_1 {
    //Создайте класс Phone, который содержит переменные number, model и weight.
// -Создайте три экземпляра этого класса.
// -Выведите на консоль значения их переменных.
// -Добавьте в класс Phone методы: receiveCall, имеет один параметр – имязвонящего.
// Выводит на консоль сообщение “Звонит {name}”.
// Метод getNumber – возвращает номер телефона.
// -Вызовите эти методы для каждого из объектов.
    public static void main(String[] args) {
        Phone phone1 = new Phone(89047689345L, "iphone", 340);
        Phone phone2 = new Phone(89172374934L, "samsung", 322);
        Phone phone3 = new Phone(89043423234L, "yandexPhone", 310);

        System.out.println("Номер телефона: " + phone1.number);
        System.out.println("Модель: " + phone1.model);
        System.out.println("Масса: " + phone1.weight);
        System.out.println();
        System.out.println("Номер телефона: " + phone2.number);
        System.out.println("Модель: " + phone2.model);
        System.out.println("Масса: " + phone2.weight);
        System.out.println();
        System.out.println("Номер телефона: " + phone3.number);
        System.out.println("Модель: " + phone3.model);
        System.out.println("Масса: " + phone3.weight);
        System.out.println();
        phone1.receiveCall("Иван");
        phone2.receiveCall("Петр");
        phone3.receiveCall("Сидор");
        System.out.println();
        System.out.println("Номер телефона: " + phone1.getNumber());
        System.out.println("Номер телефона: " + phone2.getNumber());
        System.out.println("Номер телефона: " + phone3.getNumber());
    }
}