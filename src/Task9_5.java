import java.util.Random;

//Дан массив.
//Определить:
//1.количество максимальных элементов в массиве;
//2.количество минимальных элементов в массиве
public class Task9_5 {
    public static void main(String[] args) {
        Random r = new Random();
        int n = 48;
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(-100, 100);
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        int max = array[0];
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        int maxcount = 0;
        int mincount = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == max) {
                maxcount += 1;
            }
        }
        System.out.println( "Количество максимальных элементов = " + maxcount+(" (Max = " + max + ")"));

        for (int i = 0; i < array.length; i++) {
            if (array[i] == min) {
                mincount += 1;
            }
        }
        System.out.println("Количество минимальных элементов = " + mincount+(" Min = " + min + ")"));
    }
}