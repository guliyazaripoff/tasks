public class Phone {
    long number;
    String model;
    double weight;

    public void receiveCall(String name) {
        System.out.println("Звонит " + name);
    }

    public Phone(long number, String model, double weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

    public long getNumber() {
        return this.number;
    }
}




