// Напечатать таблицу соответствия между весом в фунтах и весом в килограммах для
// значений 1, 2, ..., 10 фунтов (1 фунт = 453 г).
public class Task6_3 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + " ф. - " + (i * 453)/1000.0+ " кг.");
        }

    }
}
