import java.util.Scanner;
import java.util.Random;

public class Task9_6 {
    //Составить программу:
    //1.нахождения минимального значения среди элементов любой строки двумерного массива;
    //2.нахождения максимального значения среди лементов любого столбца двумерного массива
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random r = new Random();
        int[][] array2d = new int[r.nextInt(5, 10)][r.nextInt(5, 10)];

        for (int i = 0; i < array2d.length; i++) {
            for (int j = 0; j < array2d[i].length; j++) {
                array2d[i][j] = r.nextInt(20);
                System.out.print(array2d[i][j] + " ");
            }
            System.out.println();
        }

        int n;
        do {
            System.out.println("Введите номер строки для поиска минимального значения [0.." + (array2d.length - 1) + "]:");
            n = scan.nextInt();
        }
        while ((n < 0) || (n >= array2d.length));

        int min = array2d[n][0];
        for (int j = 0; j < array2d[n].length; j++) {
            if (array2d[n][j] < min) {
                min = array2d[n][j];
            }
        }
        System.out.println("Минимальный  элемент в строке:" + min);

        int m;
        do {
            System.out.println("Введите номер столбца для поиска максимального значения [0.." + (array2d[0].length - 1) + "]:");
            m = scan.nextInt();
        } while ((m < 0) || (m >= array2d[0].length));

        int max = array2d[0][m];

        for (int i = 0; i < array2d.length; i++) {
            if (array2d[i][m] > max) {
                max = array2d[i][m];
            }
        }
        System.out.println("Максимальный элемент в столбце:" + max);
    }
}

